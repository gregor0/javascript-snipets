// Finds a linear function (y=a+bx) that approximates a given set of points.
// It minimizes the sum of the square errors (a+bx_i - y_i)².

// Usage: 
// X, Y arrays containing the coordinates of the points
// X = [2015, 2017, 2018, 2019]; // year
// Y = [1.78, 1.85, 1.80, 1.80]; // height in year
// obj = linearFit(X, Y); //= { intersect: -5.685142857142864, slope: 0.0037142857142857177, correlationCoefficient: 0.21243077879874514, funct: funct() }
// also see https://www.wolframalpha.com/input/?i=linear+fit+%282015%2C+1.78%29%2C+%282017%2C+1.85%29%2C+%282018%2C++1.80%29+%2C+%282019%2C+1.80%29
// obj.funct(2030); //= 1.854857142857143, your approximate height in 2030 assuming linear growth

function linearFit(X, Y){
	// Based on http://mathworld.wolfram.com/LeastSquaresFitting.html
	// using snake case to indicate indicate indexes
	if(X.length != Y.length){ //enforce same length
		return null;
	}
	
	n = X.length;
	
	sum = array => array.reduce((a,b)=>a+b);
	
	xMean = sum(X)/n; 
	yMean = sum(Y)/n;
	
	dX = X.map(x_i =>  x_i - xMean);
	dY = Y.map(y_i =>  y_i - yMean);
	
	SS_XX = sum( X.map( (_,i)=>dX[i]*dX[i] ) );
	SS_XY = sum( Y.map( (_,i)=>dX[i]*dY[i] ) );
	SS_YY = sum( Y.map( (_,i)=>dY[i]*dY[i] ) );
	
	slope = SS_XY/SS_XX;
	intersect = yMean - slope * xMean;
	correlationCoefficient = Math.sqrt( SS_XY *SS_XY /SS_XX /SS_YY );
	funct = t => (intersect + t * slope);
	return {
		intersect:intersect, 
		slope:slope, 
		correlationCoefficient: correlationCoefficient, 
		funct: funct
	};
}
